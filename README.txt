Description
-----------
This module adds loginbeep passwordless login functionality to your Drupal site.

Requirements
------------
Drupal 7.x
phpseclib library

Installation
------------
1. Install this module

2. Add the phpseclib library version 1.* to the libraries folder,
   downloadable here: https://github.com/phpseclib/phpseclib

3. Login as an administrator. Set API key, secret key and public key
   under "Administrator" -> "System" -> "Loginbeep". These keys can be found
   in your Loginbeep dashboard.

