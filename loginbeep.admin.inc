<?php
/**
 * @file
 * Administrative settings form for the loginbeep module
 */


/**
 * Administrative settings
 *
 * @return array|mixed
 */
function loginbeep_admin_settings() {
  global $base_url;
  $form = [];

  $form['intro'] = [
    '#markup' => t(
      'To create an API key and secret, go to <a target="_blank" href="https://loginbeep.io">loginbeep.io</a> and create a property. <br />Use <code>@base_url/loginbeep/callback</code> when asked for a callback URL.</pre>',
      ['@base_url' => $base_url]
    ),
  ];

  $form['loginbeep_api_key'] = [
    '#type' => 'textfield',
    '#title' => t('API Key'),
    '#default_value' => variable_get('loginbeep_api_key', ''),
    '#description' => t('The API key of your Loginbeep property'),
  ];

  $form['loginbeep_api_secret'] = [
    '#type' => 'textfield',
    '#title' => t('API Secret'),
    '#default_value' => variable_get('loginbeep_api_secret', ''),
    '#description' => t('The API secret of your Loginbeep property'),
  ];

  $form['loginbeep_public_key'] = [
    '#type' => 'textarea',
    '#title' => t('API Public Key'),
    '#default_value' => variable_get('loginbeep_public_key', ''),
    '#description' => t('The public key of your Loginbeep property'),
  ];

  $form = system_settings_form($form);
  return $form;
}