<?php
/**
 * @file
 * Loginbeep API functionality
 */


/**
 * Got callback from loginbeep, check status
 *
 * @return string
 */
function loginbeep_authentication_callback() {
  require_once(libraries_get_path('phpseclib') . "/Crypt/RSA.php");

  $data = [
    'key' => variable_get('loginbeep_api_key'),
    'secret' => variable_get('loginbeep_api_secret'),
    'oth' => $_GET['oth'],
    'identity' => $_GET['identity'],
    'challenge_id' => $_GET['challenge_id']
  ];

  $result = drupal_http_request(LOGINBEEP_API_URL . "/confirm", [
    'method' => 'POST',
    'data' => json_encode($data)
  ]);

  $result_data = json_decode($result->data);

  // Check for correct data
  if (empty($result_data->payload) || empty($result_data->signature)) {
    watchdog('loginbeep', 'Empty payload or signature', [], WATCHDOG_WARNING);
    drupal_access_denied();
    return;
  }

  // Verify the signature of the confirmation payload
  if (!_verify_signature($result_data->payload, base64_decode($result_data->signature))) {
    watchdog(
      'loginbeep',
      'Signature does not match payload for login identity :identity',
      [':identity' => $data['identity']],
      WATCHDOG_WARNING
    );

    drupal_access_denied();
    return;
  }

  $payload = json_decode(base64_decode($result_data->payload));

  // Is the challenge verified and created in the last 5 minutes?
  if ($payload->verified && time() - strtotime($payload->challenge_ts) < 300) {
    $users = user_load_multiple([], ['mail' => $_GET['identity'], 'status' => '1']);
    $account = reset($users);

    if (empty($account->uid)) {
      drupal_access_denied();
      return;
    }

    $uid = ['uid' => $account->uid];
    user_login_submit([], $uid);
    drupal_goto('<front>');
    return;
  }

  drupal_access_denied();
}

/**
 * Return 200 or 404 depending on whether a specified
 * identity exists on this drupal installation.
 *
 * This is called by Loginbeep during the authentication process.
 */
function loginbeep_identity_exists() {

  // Check if identity is given
  if (empty($_POST['identity'])) {
    drupal_add_http_header('status', 403, TRUE);
    watchdog('loginbeep', 'Identity not in POST data', [], WATCHDOG_WARNING);
    return;
  }

  // Check if API secret is given
  if (empty($_POST['api_secret'])) {
    drupal_add_http_header('status', 403, TRUE);
    watchdog('loginbeep', 'API secret not in POST data', [], WATCHDOG_WARNING);
    return;
  }

  $identity = $_POST['identity'];
  $api_secret = $_POST['api_secret'];

  // API secret should be the same as given in loginbeep configuration
  if ($api_secret != variable_get('loginbeep_api_secret', NULL)) {
    drupal_add_http_header('status', 403, TRUE);
    watchdog('loginbeep', 'API secret not correct', [], WATCHDOG_WARNING);
    return;
  }

  $users = user_load_multiple([], ['mail' => $identity, 'status' => '1']);
  $account = reset($users);

  if (empty($account->uid)) {
    drupal_add_http_header('status', 403, TRUE);
    watchdog('loginbeep', 'Account uid for :identity is empty', [':identity' => $identity], WATCHDOG_WARNING);
    return;
  }

  // API secret is correct and user was found
  drupal_add_http_header('status', 200, TRUE);
  return;
}

/**
 * Verifies the PKCS1v15 SHA256 signature of message using $publicKey
 *
 * @param $message
 * @param $signature
 *
 * @return bool
 */
function _verify_signature($message, $signature) {
  $rsa = new Crypt_RSA();
  $rsa->setHash('sha256');
  $rsa->setSignatureMode(CRYPT_RSA_SIGNATURE_PKCS1);

  if (!$rsa->loadKey(variable_get('loginbeep_public_key'))) {
    watchdog('loginbeep', 'Could not load public key', [], WATCHDOG_CRITICAL);
    return FALSE;
  }

  return $rsa->verify($message, $signature);
}
